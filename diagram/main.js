import { Diagram } from './Diagram.js';
import { data } from './data.js';

const diagram = new Diagram(data, {});

diagram.render();
